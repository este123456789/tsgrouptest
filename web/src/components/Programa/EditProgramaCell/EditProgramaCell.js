import { navigate, routes } from '@redwoodjs/router'

import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import ProgramaForm from 'src/components/Programa/ProgramaForm'

export const QUERY = gql`
  query EditProgramaById($id: Int!) {
    programa: programa(id: $id) {
      id
      nombre
      descripcion
      createdAt
      id_usuario_creacion
    }
  }
`
const UPDATE_PROGRAMA_MUTATION = gql`
  mutation UpdateProgramaMutation($id: Int!, $input: UpdateProgramaInput!) {
    updatePrograma(id: $id, input: $input) {
      id
      nombre
      descripcion
      createdAt
      id_usuario_creacion
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ programa }) => {
  const [updatePrograma, { loading, error }] = useMutation(
    UPDATE_PROGRAMA_MUTATION,
    {
      onCompleted: () => {
        toast.success('Programa updated')
        navigate(routes.programas())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (input, id) => {
    updatePrograma({ variables: { id, input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          Edit Programa {programa.id}
        </h2>
      </header>

      <div className="rw-segment-main">
        <ProgramaForm
          programa={programa}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
