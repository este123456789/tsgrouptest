import { Link, routes } from '@redwoodjs/router'

import Programas from 'src/components/Programa/Programas'

export const QUERY = gql`
  query FindProgramas {
    programas {
      id
      nombre
      descripcion
      createdAt
      id_usuario_creacion
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No programas yet. '}

      <Link to={routes.newPrograma()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ programas }) => {
  return <Programas programas={programas} />
}
