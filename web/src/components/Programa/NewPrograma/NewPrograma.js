import { navigate, routes } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import ProgramaForm from 'src/components/Programa/ProgramaForm'

const CREATE_PROGRAMA_MUTATION = gql`
  mutation CreateProgramaMutation($input: CreateProgramaInput!) {
    createPrograma(input: $input) {
      id
    }
  }
`

const NewPrograma = () => {
  const [createPrograma, { loading, error }] = useMutation(
    CREATE_PROGRAMA_MUTATION,
    {
      onCompleted: () => {
        toast.success('Programa created')
        navigate(routes.programas())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (input) => {
    createPrograma({ variables: { input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">New Programa</h2>
      </header>

      <div className="rw-segment-main">
        <ProgramaForm onSave={onSave} loading={loading} error={error} />
      </div>
    </div>
  )
}

export default NewPrograma
