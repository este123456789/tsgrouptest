import Programa from 'src/components/Programa/Programa'

export const QUERY = gql`
  query FindProgramaById($id: Int!) {
    programa: programa(id: $id) {
      id
      nombre
      descripcion
      createdAt
      id_usuario_creacion
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Programa not found</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ programa }) => {
  return <Programa programa={programa} />
}
