import humanize from 'humanize-string'

import { Link, routes } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import { QUERY } from 'src/components/Programa/ProgramasCell'

const DELETE_PROGRAMA_MUTATION = gql`
  mutation DeleteProgramaMutation($id: Int!) {
    deletePrograma(id: $id) {
      id
    }
  }
`

const MAX_STRING_LENGTH = 150

const formatEnum = (values) => {
  if (values) {
    if (Array.isArray(values)) {
      const humanizedValues = values.map((value) => humanize(value))
      return humanizedValues.join(', ')
    } else {
      return humanize(values)
    }
  }
}

const truncate = (text) => {
  let output = text
  if (text && text.length > MAX_STRING_LENGTH) {
    output = output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output
}

const jsonTruncate = (obj) => {
  return truncate(JSON.stringify(obj, null, 2))
}

const timeTag = (datetime) => {
  return (
    datetime && (
      <time dateTime={datetime} title={datetime}>
        {new Date(datetime).toUTCString()}
      </time>
    )
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const ProgramasList = ({ programas }) => {
  const [deletePrograma] = useMutation(DELETE_PROGRAMA_MUTATION, {
    onCompleted: () => {
      toast.success('Programa deleted')
    },
    onError: (error) => {
      toast.error(error.message)
    },
    // This refetches the query on the list page. Read more about other ways to
    // update the cache over here:
    // https://www.apollographql.com/docs/react/data/mutations/#making-all-other-cache-updates
    refetchQueries: [{ query: QUERY }],
    awaitRefetchQueries: true,
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete programa ' + id + '?')) {
      deletePrograma({ variables: { id } })
    }
  }

  return (
    <div className="rw-segment rw-table-wrapper-responsive">
      <table className="rw-table">
        <thead>
          <tr>
            <th>Id</th>

            <th>Nombre</th>

            <th>Descripcion</th>

            <th>Created at</th>

            <th>Id usuario creacion</th>

            <th>&nbsp;</th>
          </tr>
        </thead>

        <tbody>
          {programas.map((programa) => (
            <tr key={programa.id}>
              <td>{truncate(programa.id)}</td>

              <td>{truncate(programa.nombre)}</td>

              <td>{truncate(programa.descripcion)}</td>

              <td>{timeTag(programa.createdAt)}</td>

              <td>{truncate(programa.id_usuario_creacion)}</td>

              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.programa({ id: programa.id })}
                    title={'Show programa ' + programa.id + ' detail'}
                    className="rw-button rw-button-small"
                  >
                    Show
                  </Link>

                  <Link
                    to={routes.editPrograma({ id: programa.id })}
                    title={'Edit programa ' + programa.id}
                    className="rw-button rw-button-small rw-button-blue"
                  >
                    Edit
                  </Link>

                  <button
                    type="button"
                    title={'Delete programa ' + programa.id}
                    className="rw-button rw-button-small rw-button-red"
                    onClick={() => onDeleteClick(programa.id)}
                  >
                    Delete
                  </button>
                </nav>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default ProgramasList
