import humanize from 'humanize-string'

import { Link, routes, navigate } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

const DELETE_PROGRAMA_MUTATION = gql`
  mutation DeleteProgramaMutation($id: Int!) {
    deletePrograma(id: $id) {
      id
    }
  }
`

const formatEnum = (values) => {
  if (values) {
    if (Array.isArray(values)) {
      const humanizedValues = values.map((value) => humanize(value))
      return humanizedValues.join(', ')
    } else {
      return humanize(values)
    }
  }
}

const jsonDisplay = (obj) => {
  return (
    <pre>
      <code>{JSON.stringify(obj, null, 2)}</code>
    </pre>
  )
}

const timeTag = (datetime) => {
  return (
    datetime && (
      <time dateTime={datetime} title={datetime}>
        {new Date(datetime).toUTCString()}
      </time>
    )
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const Programa = ({ programa }) => {
  const [deletePrograma] = useMutation(DELETE_PROGRAMA_MUTATION, {
    onCompleted: () => {
      toast.success('Programa deleted')
      navigate(routes.programas())
    },
    onError: (error) => {
      toast.error(error.message)
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete programa ' + id + '?')) {
      deletePrograma({ variables: { id } })
    }
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            Programa {programa.id} Detail
          </h2>
        </header>

        <table className="rw-table">
          <tbody>
            <tr>
              <th>Id</th>

              <td>{programa.id}</td>
            </tr>
            <tr>
              <th>Nombre</th>

              <td>{programa.nombre}</td>
            </tr>
            <tr>
              <th>Descripcion</th>

              <td>{programa.descripcion}</td>
            </tr>
            <tr>
              <th>Created at</th>

              <td>{timeTag(programa.createdAt)}</td>
            </tr>
            <tr>
              <th>Id usuario creacion</th>

              <td>{programa.id_usuario_creacion}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <nav className="rw-button-group">
        <Link
          to={routes.editPrograma({ id: programa.id })}
          className="rw-button rw-button-blue"
        >
          Edit
        </Link>

        <button
          type="button"
          className="rw-button rw-button-red"
          onClick={() => onDeleteClick(programa.id)}
        >
          Delete
        </button>
      </nav>
    </>
  )
}

export default Programa
