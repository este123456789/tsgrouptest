import { Link, routes } from '@redwoodjs/router'

import Estudiantes from 'src/components/Estudiante/Estudiantes'

export const QUERY = gql`
  query FindEstudiantes {
    estudiantes {
      id
      cedula
      nombre
      email
      telefono
      createdAt
      id_usuario_creacion
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No estudiantes yet. '}

      <Link to={routes.newEstudiante()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ estudiantes }) => {
  return <Estudiantes estudiantes={estudiantes} />
}
