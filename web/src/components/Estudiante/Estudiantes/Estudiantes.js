import humanize from 'humanize-string'

import { Link, routes } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import { QUERY } from 'src/components/Estudiante/EstudiantesCell'

const DELETE_ESTUDIANTE_MUTATION = gql`
  mutation DeleteEstudianteMutation($id: Int!) {
    deleteEstudiante(id: $id) {
      id
    }
  }
`

const MAX_STRING_LENGTH = 150

const formatEnum = (values) => {
  if (values) {
    if (Array.isArray(values)) {
      const humanizedValues = values.map((value) => humanize(value))
      return humanizedValues.join(', ')
    } else {
      return humanize(values)
    }
  }
}

const truncate = (text) => {
  let output = text
  if (text && text.length > MAX_STRING_LENGTH) {
    output = output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output
}

const jsonTruncate = (obj) => {
  return truncate(JSON.stringify(obj, null, 2))
}

const timeTag = (datetime) => {
  return (
    datetime && (
      <time dateTime={datetime} title={datetime}>
        {new Date(datetime).toUTCString()}
      </time>
    )
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const EstudiantesList = ({ estudiantes }) => {
  const [deleteEstudiante] = useMutation(DELETE_ESTUDIANTE_MUTATION, {
    onCompleted: () => {
      toast.success('Estudiante deleted')
    },
    onError: (error) => {
      toast.error(error.message)
    },
    // This refetches the query on the list page. Read more about other ways to
    // update the cache over here:
    // https://www.apollographql.com/docs/react/data/mutations/#making-all-other-cache-updates
    refetchQueries: [{ query: QUERY }],
    awaitRefetchQueries: true,
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete estudiante ' + id + '?')) {
      deleteEstudiante({ variables: { id } })
    }
  }

  return (
    <div className="rw-segment rw-table-wrapper-responsive">
      <table className="rw-table">
        <thead>
          <tr>
            <th>Id</th>

            <th>Cedula</th>

            <th>Nombre</th>

            <th>Email</th>

            <th>Telefono</th>

            <th>Created at</th>

            <th>Id usuario creacion</th>

            <th>&nbsp;</th>
          </tr>
        </thead>

        <tbody>
          {estudiantes.map((estudiante) => (
            <tr key={estudiante.id}>
              <td>{truncate(estudiante.id)}</td>

              <td>{truncate(estudiante.cedula)}</td>

              <td>{truncate(estudiante.nombre)}</td>

              <td>{truncate(estudiante.email)}</td>

              <td>{truncate(estudiante.telefono)}</td>

              <td>{timeTag(estudiante.createdAt)}</td>

              <td>{truncate(estudiante.id_usuario_creacion)}</td>

              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.estudiante({ id: estudiante.id })}
                    title={'Show estudiante ' + estudiante.id + ' detail'}
                    className="rw-button rw-button-small"
                  >
                    Show
                  </Link>

                  <Link
                    to={routes.editEstudiante({ id: estudiante.id })}
                    title={'Edit estudiante ' + estudiante.id}
                    className="rw-button rw-button-small rw-button-blue"
                  >
                    Edit
                  </Link>

                  <button
                    type="button"
                    title={'Delete estudiante ' + estudiante.id}
                    className="rw-button rw-button-small rw-button-red"
                    onClick={() => onDeleteClick(estudiante.id)}
                  >
                    Delete
                  </button>
                </nav>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default EstudiantesList
