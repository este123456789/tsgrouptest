import { navigate, routes } from '@redwoodjs/router'

import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import EstudianteForm from 'src/components/Estudiante/EstudianteForm'

export const QUERY = gql`
  query EditEstudianteById($id: Int!) {
    estudiante: estudiante(id: $id) {
      id
      cedula
      nombre
      email
      telefono
      createdAt
      id_usuario_creacion
    }
  }
`
const UPDATE_ESTUDIANTE_MUTATION = gql`
  mutation UpdateEstudianteMutation($id: Int!, $input: UpdateEstudianteInput!) {
    updateEstudiante(id: $id, input: $input) {
      id
      cedula
      nombre
      email
      telefono
      createdAt
      id_usuario_creacion
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ estudiante }) => {
  const [updateEstudiante, { loading, error }] = useMutation(
    UPDATE_ESTUDIANTE_MUTATION,
    {
      onCompleted: () => {
        toast.success('Estudiante updated')
        navigate(routes.estudiantes())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (input, id) => {
    updateEstudiante({ variables: { id, input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          Edit Estudiante {estudiante.id}
        </h2>
      </header>

      <div className="rw-segment-main">
        <EstudianteForm
          estudiante={estudiante}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
