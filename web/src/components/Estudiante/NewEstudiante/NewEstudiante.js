import { navigate, routes } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import EstudianteForm from 'src/components/Estudiante/EstudianteForm'

const CREATE_ESTUDIANTE_MUTATION = gql`
  mutation CreateEstudianteMutation($input: CreateEstudianteInput!) {
    createEstudiante(input: $input) {
      id
    }
  }
`

const NewEstudiante = () => {
  const [createEstudiante, { loading, error }] = useMutation(
    CREATE_ESTUDIANTE_MUTATION,
    {
      onCompleted: () => {
        toast.success('Estudiante created')
        navigate(routes.estudiantes())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (input) => {
    createEstudiante({ variables: { input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">New Estudiante</h2>
      </header>

      <div className="rw-segment-main">
        <EstudianteForm onSave={onSave} loading={loading} error={error} />
      </div>
    </div>
  )
}

export default NewEstudiante
