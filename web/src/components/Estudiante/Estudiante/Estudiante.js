import humanize from 'humanize-string'

import { Link, routes, navigate } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

const DELETE_ESTUDIANTE_MUTATION = gql`
  mutation DeleteEstudianteMutation($id: Int!) {
    deleteEstudiante(id: $id) {
      id
    }
  }
`

const formatEnum = (values) => {
  if (values) {
    if (Array.isArray(values)) {
      const humanizedValues = values.map((value) => humanize(value))
      return humanizedValues.join(', ')
    } else {
      return humanize(values)
    }
  }
}

const jsonDisplay = (obj) => {
  return (
    <pre>
      <code>{JSON.stringify(obj, null, 2)}</code>
    </pre>
  )
}

const timeTag = (datetime) => {
  return (
    datetime && (
      <time dateTime={datetime} title={datetime}>
        {new Date(datetime).toUTCString()}
      </time>
    )
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const Estudiante = ({ estudiante }) => {
  const [deleteEstudiante] = useMutation(DELETE_ESTUDIANTE_MUTATION, {
    onCompleted: () => {
      toast.success('Estudiante deleted')
      navigate(routes.estudiantes())
    },
    onError: (error) => {
      toast.error(error.message)
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete estudiante ' + id + '?')) {
      deleteEstudiante({ variables: { id } })
    }
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            Estudiante {estudiante.id} Detail
          </h2>
        </header>

        <table className="rw-table">
          <tbody>
            <tr>
              <th>Id</th>

              <td>{estudiante.id}</td>
            </tr>
            <tr>
              <th>Cedula</th>

              <td>{estudiante.cedula}</td>
            </tr>
            <tr>
              <th>Nombre</th>

              <td>{estudiante.nombre}</td>
            </tr>
            <tr>
              <th>Email</th>

              <td>{estudiante.email}</td>
            </tr>
            <tr>
              <th>Telefono</th>

              <td>{estudiante.telefono}</td>
            </tr>
            <tr>
              <th>Created at</th>

              <td>{timeTag(estudiante.createdAt)}</td>
            </tr>
            <tr>
              <th>Id usuario creacion</th>

              <td>{estudiante.id_usuario_creacion}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <nav className="rw-button-group">
        <Link
          to={routes.editEstudiante({ id: estudiante.id })}
          className="rw-button rw-button-blue"
        >
          Edit
        </Link>

        <button
          type="button"
          className="rw-button rw-button-red"
          onClick={() => onDeleteClick(estudiante.id)}
        >
          Delete
        </button>
      </nav>
    </>
  )
}

export default Estudiante
