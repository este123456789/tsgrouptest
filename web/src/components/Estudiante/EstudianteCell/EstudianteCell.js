import Estudiante from 'src/components/Estudiante/Estudiante'

export const QUERY = gql`
  query FindEstudianteById($id: Int!) {
    estudiante: estudiante(id: $id) {
      id
      cedula
      nombre
      email
      telefono
      createdAt
      id_usuario_creacion
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Estudiante not found</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ estudiante }) => {
  return <Estudiante estudiante={estudiante} />
}
