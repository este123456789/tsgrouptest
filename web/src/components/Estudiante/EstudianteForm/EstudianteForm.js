import {
  Form,
  FormError,
  FieldError,
  Label,
  TextField,
  NumberField,
  Submit,
} from '@redwoodjs/forms'

const EstudianteForm = (props) => {
  const onSubmit = (data) => {
    props.onSave(data, props?.estudiante?.id)
  }

  return (
    <div className="rw-form-wrapper">
      <Form onSubmit={onSubmit} error={props.error}>
        <FormError
          error={props.error}
          wrapperClassName="rw-form-error-wrapper"
          titleClassName="rw-form-error-title"
          listClassName="rw-form-error-list"
        />

        <Label
          name="cedula"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          Cedula
        </Label>

        <TextField
          name="cedula"
          defaultValue={props.estudiante?.cedula}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{ required: true }}
        />

        <FieldError name="cedula" className="rw-field-error" />

        <Label
          name="nombre"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          Nombre
        </Label>

        <TextField
          name="nombre"
          defaultValue={props.estudiante?.nombre}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
        />

        <FieldError name="nombre" className="rw-field-error" />

        <Label
          name="email"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          Email
        </Label>

        <TextField
          name="email"
          defaultValue={props.estudiante?.email}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{ required: true }}
        />

        <FieldError name="email" className="rw-field-error" />

        <Label
          name="telefono"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          Telefono
        </Label>

        <TextField
          name="telefono"
          defaultValue={props.estudiante?.telefono}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
        />

        <FieldError name="telefono" className="rw-field-error" />

        <Label
          name="id_usuario_creacion"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          Id usuario creacion
        </Label>

        <NumberField
          name="id_usuario_creacion"
          defaultValue={props.estudiante?.id_usuario_creacion}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{ required: true }}
        />

        <FieldError name="id_usuario_creacion" className="rw-field-error" />

        <div className="rw-button-group">
          <Submit disabled={props.loading} className="rw-button rw-button-blue">
            Save
          </Submit>
        </div>
      </Form>
    </div>
  )
}

export default EstudianteForm
