// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

import { Set, Router, Route } from '@redwoodjs/router'

import ProgramasLayout from 'src/layouts/ProgramasLayout'

import EstudiantesLayout from 'src/layouts/EstudiantesLayout'

import BaseLayout from 'src/layouts/BaseLayout'

const Routes = () => {
  return (
    <Router>
      <Set wrap={BaseLayout}>
        <Route path="/programas/new" page={ProgramaNewProgramaPage} name="newPrograma" />
        <Route path="/programas/{id:Int}/edit" page={ProgramaEditProgramaPage} name="editPrograma" />
        <Route path="/programas/{id:Int}" page={ProgramaProgramaPage} name="programa" />
        <Route path="/programas" page={ProgramaProgramasPage} name="programas" />
      <Route path="/programas" page={ProgramasPage} name="programas" />
        <Route path="/estudiantes/new" page={EstudianteNewEstudiantePage} name="newEstudiante" />
        <Route path="/estudiantes/{id:Int}/edit" page={EstudianteEditEstudiantePage} name="editEstudiante" />
        <Route path="/estudiantes/{id:Int}" page={EstudianteEstudiantePage} name="estudiante" />
        <Route path="/estudiantes" page={EstudianteEstudiantesPage} name="estudiantes" />
      <Route path="/estudiantes" page={EstudiantesPage} name="estudiantes" />
      <Route path="/" page={InicioPage} name="inicio" />
      <Route notfound page={NotFoundPage} />
     </Set>
    </Router>
  )
}

export default Routes
