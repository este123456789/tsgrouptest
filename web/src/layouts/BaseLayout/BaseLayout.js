
import { Link, routes } from '@redwoodjs/router'


const BaseLayout = ({ children }) => {

  let pathname = (window.location.pathname == '/') ? '' : window.location.pathname.replace('/', '__')
  let mainClass = 'main' + pathname
  let idBlock = (pathname.replace('__', '') == '') ? 'inicio' : pathname.replace('__', '')



  const changeContent = (event) => {
    // this.setState({inputContent: e.target.value})
    let val = event.target.value
    let filter = event.target.value
    let tr = document.getElementsByTagName('tr')
    let td = document.getElementsByTagName('td')

    for (let i = 1; i <= tr.length; i++) {
      let a = document.getElementsByTagName('tr')[i].textContent
      if (a.indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }


    }


  }
  const onToggle = () => {
    let toggleMenu = document.getElementById('toggleMenu')
    if (window.getComputedStyle(toggleMenu).display === 'block') {
      toggleMenu.style.display = 'none'

    }
    else {
      toggleMenu.style.display = 'block'
    }
  }


  const handleInputChange = (e) => {
    let target = e.target
    let value = target.type === 'checkbox' ? target.checked : target.value;

    alert(value)
    // const value = target.type === 'checkbox' ? target.checked : target.value;
    // const name = target.name;
    // this.setState({
    //     [name]: value
    // });
  }

  return <>


    <header>
      <nav className="flex items-center justify-between flex-wrap bg-slate-200 p-4">
        <div className="flex items-center flex-shrink-0 text-white mr-6">
          <svg xmlns="http://www.w3.org/2000/svg" version="1.2" viewBox="0 0 195 73" width="90" height="49" className='logo'>
            <title>company-238324-1594926738</title>
            <defs>
              <image width="195" height="72" id="img1" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMMAAABICAYAAABLCEGxAAAAAXNSR0IB2cksfwAAN8lJREFUeJztXQeYFdXZnlhA+oLdaCSaRGNFY0ksxBa7BltU2A7YBRQFbIgIIlbsXbCgUlRIAAsWEBGR3peFbXfb7W339pl5//Oeuecye91ll8gvPnF5no/Ze++5Z+bOfO/5+nc0ANp2yTQ1GIb4O6UZMATp4u+4hhQ0w0hpupkUlNKQCGtJ8TqRjGmpUN2kxvKF8K/8GL4lUxFY9CaCS95CeMNniLo2IZps1CJmg5bUY5qpi/kS0JLiPEbKlPPCtChhGloCxvavr53aaSdR64MEQ8JIaLpgyiRMLSVfC+bXDcnIhkEwNGgI100yKr5Hcs6z8D5VgOoHTkXV8GNRefufUXX7Uai44xjUjDgV3gmXI/zmbUgufwemex2MuOeslACFIeZLGKYAiDgnSYDMMFNp8O36G9VO//vU6gBTML/B1d+AAIIuVnDBoEJSmEldM1MpLRXz5zTUrYFrwUfwvXM/qu89B478g+Au6gh3Xkd48rugviAHjuJucOV3hTO3KxyFvVA68ChsHHsFYosnIxUoFbyfFEAQkkXMbQgwmAJkppmQoNjVN6mdfh3U6gDDpAojmDNFBo0LQMS0KKVDFFo8FtSqN36DwIK3EfhmOoLv3YlSIQ1qCvcVQNgTgfxucOf2QHXBPigd3EO83xmugo6oK9gT4X8diLqinii98whsnTwcQa+r0ExGND0VE+qRkEBxni8mwBDd5TepnX4d1OoAXQAhSrXFCAjGTGhJsYKbekJLBOsmNSz9FOGVcxDcPAeJl4vhHX4Y6oVE8OV1he/6ngiS+veAN7eLYPwu8BR2hTe/M7x5eyGQ2wHeAZ3h6d8FQSEtPGMuQGjtLKGVBQQIxPkSCS0mJE/8F3CT2unXQa0OMAQlyJxGo1BbqNvHNTMW1UKLP0RozTwYJZ8g9NEEBEYcA1dRNzgLuqBy0EHYdNtxWH/XqVg58jQsvfcMLB95JjYO7YPKwb0FCLojIMiZ3wt1ed0QEFKkZmBPbB5/OcyKr4Vm5O2DlLBThMqU0nf9TWqnXwe1OsAUKotO41aoLyk9rJlxf07dmqWIrPkUxtb58M58CLX3n4/awu6oKtoXzlGnwPfGTTCXvw/TuQhGdB0QXQ8E1sEs/QSpjx5G8NGrUHnDwXAI9cmV1x3BAbujLr8DNg06FFWT8pGo+VZIiKgAHu2TXX+T2unXQa0PoltVqEYB8XciFdJilSvhWzgd0ZoSBOe/gspxl6Duhv2EwSxW+YcvRXj1R0iGqxdAMLEuie5SQ7pPDbpjkzEt4ixH9cxC1N/eGw1CQjRe2wn1+T1RV7gPym49Ev6Zo6DDn5MyElqqHQzt9DNR64NoLCfjWoP4O5Vw96v+fhaCm76APmMs4mPOQW3B/nAWH4Tyt0bB8FWgrSc2UgEtunwWNt/1d4Ryd0dZUY6QLj2EhOgO/62HoLFSSJRU1Dr/L+BGtdP/PrU+SK7suvT/6661qJj9BBLfvIbGCRfDeXNvbB14CBzj/ylUorWAHmnziU2x6qOxdkrj3GdQcdsRqM7rIoDVA7X5PeAszIFD2CFGxDnMYNDvF3Cj2ul/n1ofpNyqCV3zfv4aUrNGw/1Uf9TedCAcZFphIwRXzUQy1SADcG09sSnm1ZMRLeUrQe1bdwpDuitcxb1Qk9sN7vxuqH74fOiOlUj9Am5SO/06qNUBZFq6OlOBoOZ5935EJl0pdP3j4C78DaoLuyHy+mAkG+vHhMS4HWFcXUiaiPhOJBHSoiVfo75ob6Ei5aAutwvcuZ3gv+UgRFZ90g6GdvrZqNUBMiXCjGiNlT/A/fR1qL/1cNTk7Qt/4Z7YdNMfYf4wFfT4EDAETltPbCahxYyUFjcTWizi7ROccImc15fbHR4BCs/gveGc86JQvRK7/Ca106+DWh1gQtcMxDTfuv+gfOQZqC7YV6zge8NX0BnLhpwM07sKOtUoYeyazFlq68kNC2gpM64ljZjmeftWVBUcDF9/AYS8nnAM6oWKN+8BEoFdfpPa6ddBrQ5gbpJpJjXfymkoH9IHTsGozvzucA3ohJX3XwQzUiYlA4zwDuURmcIwNwUYOHdSkHvW/SgvPBTe/r0kGCoH9sDWZwYD0bpJu/omtdOvg1oflLYZfMveQ9UtR8MvDFxP3l7wCyN31dh/woxXBk05Niio7WAwYMqAHkGREoa3a86D2FLUW9gLveDO64G6wV1Q9eQAIFIZ3NU3qZ1+HdTqANoBzCL1rBBq0pDjZRYqc4tcAzpgxdCTYAbWQDeZ3q1LBm/riWNMxpO1EpA1DJ7pw1BWtB9cYv66vO5wD+wCx0u3ALH6Mbv6JrXTr4NaHSBXb8HsoZKFqLjvb6gt7CwkA9OyO2PjrcfCLPkEXN11jttRMDDWkEpoZhSa/9lr4SjuKcDQUZxDqGIDhW3ywXhhMwR3+U1qp18HtT6INoNQY5KuUtRMvBiOgi7wCzDUFHbFlkGHQP9wrNDrG2V2q5GuUCM45JEEC0xyHlkbYWi6mJdBPEMYzmYypEWc9WOqhwp7pKCrkDp7wlkkQHHjH9C44D3AiO/ym9ROvw5qfZChi9U7pumJiBZ+4yYZJW7ov4dYxXuhhlmq4y9Co2Mz4oYFBlNPG8fqtQSDLtO/aTBTpZJp2QnOHdOMRP2YsiUfwZm7H4L9OyI8YA/4BNjcd5wOY+M3gNle6dZOPw+1OoAp3PF00p2xaibW3Xgk6oVO78vdHbX5XVFxw+/gfa0AerRBiwjgNMqquJBg/rCwCZLyu3FKDNY2p5IySp2klEgw1tCgRdfOQtU9Rwkp0wmOgZ0E2LrBlXcgtrw5FLFAGWLYAXdtO7XTT6BWB8h6hrTqY4Yqg5sezRUSge7VbqjOYznnfqi7/XCh0ryJVEP1glQyrAlkaEjGpHrFZgEJQXFZNsr6BGFop1JCgvh6m3WLUffktXAX9hCGeWeZzl1fsC9qB/VG/bLPoafCQqVqlwzt9PNQqwPYEUPn6qwLOyAa1JJLZ6Js2DGoZ2EO064LmG3aDSXDTkHo4ycBVwnQ6O0DWcucTAfWEtJgTvI1M1Eb3f3C6z9E3dPXYUvRb4Va1BmB3B7w5/ZEbfHvEHzqKpiRRgGYpNauJrXTz0VtGJSSKo/lAhXM7N2KrVPukrEAD2ucCzoLw7eDUJn2wdabT0LNq6MQXfsNGhsbtYSebiCgxy0wNXj7JGvWou7rKagcdSZc4jv+3M6oK+yOutx94c3NgW/UKWhY9jYsu8O0WtX8Am5UO/3vU6sDKBlSDKbpVvANgrFT/ppZ1Q9fCFdhDhoHaAjlakLXPxCVxd1RPqg7qm44AIFxF8A/+TZ4Zz4Iz38mwvn+SPheHCAM42PgLegJzwBKli5wFnWWQbyaor2wZsTxCHz7sjiFPwdmUEiRhAXCX8CNaqf/fWp1AANpSTbyEmpPnH2N2DAs1ahF1nyG6glXYmvx/vAV7gnXgC5wiVW+Ps9a6dkOprxgb1QOPAhVgw/F1qKDUVV8kOycwQKeesH8zsIOwkbohJq8nii781hUfzQWZrQymNITUkUyWSHXDoZ2+pmo1QGyBprd7ugSFceYycZhCc1IRDSjYgkczxbDccvvxArfA8GC7vALULjzugrViXUJe8FX0FGoQh2kgexksC6fTQO6C9pTgKIrHIMORv19fRFd+JLQxspgpCKyNQ2j3rJstL0hQDv9TNT6IJkyYcpaBXqWdJlGYXmYWJ8cq9+M0PR7UDv8KJl6Hbi+s1SBnPn7yqZhrrw94CnQhBTYXZCQBvlWBLvh+i7wFh+J0ON5aPxhMhICBGxBo+spATgrKCePv4Cb1E6/Dmp1gCruAVtIyg53ugBHUjBpUoBBlys44tCiFetR958nUP7opSi740i4btgXNcX7oVaoSc5BhwhVijUK+6Hult6oHnkySt4Zhbpln8JoDFqSwIhqZoI9Ww3ZutIHvXdcAATJtpeStlM7/RQS/1neGmaeGjYy06SrjnpmPN3u0Wr5qEvD2pSfsbeRnoprRrRukulYitjKjxGYMwlVbz+AzS8Px5aX78LW1+6GY/p4NC56C/rm+UDIsRrJRlkDIc9HW8RI2whC+iTE3GxszM9ZUyHfNyy1zXL3GunMV1iNitOJguraIRsWG01+n/W+Kcfu6hvfTr880rYxi5lmJjPDZKZpay8pJIJkWNYsyDoEI60yWUTQpGRVXMJqC5kMiVXe1xtxz1mIufvJv1NBwdCUMLE2X6B1XQoMdLcSBARjUr5PcCgw6GKsjjTTp/OhlGt2GxisjuK7+sa30y+PbC+MpqSS7tKEDJlpJktZKpNiOsOyIVJKrWKr+VRSZqUyt0n+Ld4zddNSrdp4gVzpZR20PLeRLiBi/9VGTbbGl9ewbew2MNgI7WBop9YpzSCkVJrBFSkmN6y8IsNSmciYmSJ9qdokLdKt7twco6fHcG+FhFixk3LVTqs1O3qRGQCQBJhgZOYn6XJO1VvJDmjr+9tA0Pzn7dROimyMotuAoKclQLo004gI9UeXapFM1U7r7zLlgv1QBcOyM3eKrerTpKffp12RyqheZia9u60XaLl2jfRGKXTxmvL8kkxLhTIFkKVEIGhYIyF/g0ohbwdDO7WNmjCKKXVwXcYUpEFtbuuOocuNQ5KyBkHaAw11kxCsDBreLUi5SxB1lUB3bgScmwDxGu5SGIJ0kqsUptsiuEqt/CVP28j0bBLf3wDDWwIjUFVpJEJS1TJlZ3Bhn6TE68baKXrYOcxoFPZJqlFGyWGzeWQquWkZ5IGAr7fX6znL6/X2aYk8HutzdWyJAoGAput6ugDKIt5LLhDxeFxraGjQQqGQ5vP5etvnyp5fHZnCYp/HPq9pS0uxzx8OhzVeR2vXynFGevHa1Uz3S6UmLwxZfGOtuEmx8ibTqpEpg19JLdbo7hfzb4F38+dwfPoYKj4Yjk3P9MfmR/qhbFw/lD5ypfjbotLxVwu6RtC/UDruWmwWf5c8cg02TSBdjY0T+7WJSidcDsfDF2LDxMuw6uXbENvyrdCYUhYYot4+qaof4Pv2bfi+nIzaJfMQd1cGzUSD3AmIDZP5e/S0F8rv8/YZN24sCgoKmqXCwkLk5+dn/s7Ly5PHlsaPHTsWZDQ7w5IpV65cienTp+PRRx/FmDFjcNttt8nxau6ioqIm5yLdcMMN+OCDD2CfS24Kk/6boEulUlpVVVXld999h7fffhsTJ07EQw89hDvvvLPJXNnE3/HSSy+BoFFz7mrG+yWSplQGpU6kyEDpJDmTW1UlIloqUBmMr5+L5Pyn4J14OZy3/UHuweDP7QSvIJnOzQZgBYw8d4abezDkdhGfCxogxg3oJgNtLrmLTxfUFnaBh40F2kD+Ad3RcF0P1BXmYM3wE4D1H4hL9ufodMn6tqJs1gsIzXse8e9mI7p+AaoWz0XEXSFMoLhVVaeKjITEq3E4Vp9+2uniV+8mSGuRfvOb3zQ5tjTmpJNOQl1d3SQyF1fpzz77DJdeein22Wcf+fluu+3WZI7m5lPvde7cGXfddReyQUAiCDZs2CCZ/+STT0anTp3k3NnX2tL18v1//etfmWttB0MLYDDTRq3ywkidnx4gPSrUD39Oqm4LEiWL0fDJswg+l4/aYcfCVdgLgQGd4ONmI7kCAHkCCPk9ZOJdPdMuWJuQ3wnuvG3kSr/Pz+sImNzubSJfLqPaOXIrrLXDT4K5ZrpgmKjMiE2VLYVnziT437oDkdmPIbl2HvxrvkJw6yoLDFKFUR4xXautrl5ggWGP7YKhLUQG+8tf/oLaWqGiifPMmTMHf/3rXzMAaI45tweyLl26YMSIEVBAUEcCYf369bjyyiuRk5Pzo+8r0LV2rQRDfX39mHbJsF0w2FySXEmT3MkzqiU9FQiXr0aiVABh/quoeew6VAw5BjVF+4hVv6OUCu68boLBc4Q06CmkQRcr/SJNzoJOGaov7CQ3MXHlW+QWn7vzu7eJWPDjGtADroJuWD+sjwDDDNBukTXUW5chNPdZeCYPQd2Mh9CwahZCy+chVLIUtBsMuQGjWmF1LSz096nvvocJjz6Oxx//Md1777045JBDMkx00EEH4b777mt2LFfpd955B7QJNm3aJFdsxaQdOnTAUUcdheuuuw7jxo2TYx977DH5PR4VqfdJTz31FL766qsfSYZVq1bhsssuy1zT7rvvLqXIMcccg/79+0s1zD5P9nXyvSeeeAKzZ89GMBjMAG1XM94vkTJSQYKBqkeiUYsF6iaFS5ZAr1gG7ycvwvXYVXDccrhQVViwvzsCebtJdae6oAccLO4RjOoX7/uEWuPN7SmkRQ8Bki5ivKUSsaMGXxM83lwm8/WQ6lRbiHNUF7FuogO2DD0aWDkVTNFge0qzZgtcs19BbMFraKhaBN/SD+Fc8D4itVY7e5n5aiLNXJaha6TtiOZuBvXxU045RTIdV9vjjz8eNTU1s5obq5iKRu/LL78sAcDv7bHHHlJ9mj9/vvyu3PYryxBuziZQxq1iVEqbRCKhjRw5UkoEJWn23ntvFBcX49tvv5VSifPb59netdrPtasZ75dI1va1SGhRWIZmyls3Kb7mEyQ2fI7YjPvhvv801N20L9xFLMsU0kAAoH7gAaga+idUjTgaNWNORd24c1H74JVwjL1YGLsXCLpIUtXDFwu6RPx9Sfo98dm48wX9AzVjL2gTVY89F9XjzhTHS1H69K0wS4QBbVjXmxKgaNiyAr7FMxFa9xG2LvsMgfoKENCMfchUDiM79YIM0zwz2MFAOu6447YLBhI/v+qqqzLMut9+++G1115DNoNmM2q2xyh7Xn5/+fLl+NOf/pRRvfbaay88/PDD0mhvbo7mwNASEBXYYrGYJHqn1N98v7nxdlDZwcvPaDNR8ijia7UQZNtAdlLXEI1Gf3ReXr/9fJyP49Q1ZgOcn6u51Lhs4u/M/p76W4KBOUcxgiHZqDWsWwhjxQyE3x+BmtFnwXHDwQIAHeEoPBCbbzoBleOuQ/07Y9CwZBqMDZ8BFd/BLF8KvXINzMrlQOUyQcvTtELQyjSpz0hL05+1TqYYb1aJ8VUrkKreCCPiz6FhnNCtTdnBVPKgqzDudaxOht39wOQ+xhokGHRpA/1/gEE9qK1bt4LfofrC7xx22GH45ptvYGee7YGhuZWbxIf5yiuvoGPHjhkwUOJs3rwZ2/tua2BQ77lcrsKPPvpIqnpTp06V3ikS//74449BINoN7ubAQHuGv/+LL77AM888I4FKte3BBx+Uat/cuXNRUVEBxYDNMSG/++6778pz82+7JM0e/8MPP8jrJX3yyScg4Oyf08umfgvn5Dge7TRt2jSpjpaVlYHgaAIGSIaJCx08oSU2f4/Il2/C98YQseofh6obD0ZVfi84i/eD76kB0BdOge7dDCPGPKOYMLKTmQBbApZrtkmKhmna0iK29U6yVBa0iWRgLR10k3lKpnU+QwX4dIusICALj+IWUSqY6Ryr/0fJsGXLFpx44okZyXDwwQfjww8/RHNM2NJKnS0heKSxO3DgwMy1dOvWTdovSi2yg6w5VcvOfGr1to9dsmQJevfu3cTjZf+7V69eOP/88zFv3jxEIpEfnYO/m7YKbRcC1m7Mqzn4Pu8N7SalMmZf3z/+8Q/sueee8jsXXHCBdP82p0ryeOutt2bOQ2cFgWi/JnW/sh0VzR2PPfZYDB8+HE6nc5g6h9UXSaykiXD9mNDnr8H78k2oGHka/IW7Cx2/gzCYD4Rn/FWICp08kfScFTeTMv9I9j1KWapVRDwUuc1Vkxym5kkxp95mIgiSmSi2kQGTnl6d0hFxMw0GPZFOETH/38HAI1eYv/3tb5mb3LVrV9x8880Zn35L3hv7Q1av7cxNCdC3b9/MtRBk7733npQ46rv8e0fUJPs5vv/+e/zud7/7EaMozxSPlHZnn302li5d2gTctFVovNN+sXuy6PIlaJX9pIg2zy233JJhXjsRDLSzOI5/0yHRnAThkXOo62wJDM25sltyadMRcffdd0tASDDo6dU1/P078Dx2BWpvPxKO4gMQu06Di9tKPX41Qs5NYOEN6xeS1POkHp60coYk4+lpBlU5RHZKZci00TbpsX2iJGGfJdZgI2VKaYR0XyZKJykdBMPHwa1yraQ96+Y0l46x8yUD9fc77rijyQ0nIMgszz77rBTnixYtwuLFi5sQPVCc2+/352TrvySqKUceeWSGMf/85z9L1SVb3yZj8rodDsdqHpsj8S/IYCDVGvVdBQbFGLxeruBDhw7FWWedJRma5+Xx/vvvlyAkUWINGTJErubKYcDr5D149dVXpZpClWnAgAFNwEZ7h8xMZrczOaUP58iWDNn3ww4GEhcg/i77mEGDBmV+D9VVqmuTJk2SKhuvacKECbj++uulRLRfl/p9gsGt4Fr49WLUDDkCjoJeqMnviWDuntg86DCkvnhB8HpUVqFxK1rmCbHWIAaLSY000+qyW54uc5hITJ6TnfTSuUAmjEwdgg6lQrVOpgKfrrr1GbL1DMk6l5UvJcFgGmnpYzYBQhMwqLl3Ahj4gMiQ//73v+UqaV+F+ICpavzhD3+QjExXq/3IGMW5554LeosYo2C6hP3Bf/rpp/KB2q+FALI//JKSElx99dVylSSdeuqp8khG4d/qNY8PPPBAk2g51SQ7GKZMmSLVIYKToKP6o34LGUhdH+2Mww8/PPMZ52a0XaWmKJWsurp6DB0J+++/f0Z67LvvvqCr2K6yKTDwc/6t3L/NqZFKTVKSgVI5GwwKLKeffjrBUmm/p7RdxKKxgLYDA6NqoeF94Hm1WEpcWDio1d/aW+626S7kZuU9UC+kwoZR/4Dp2wC5K49BIyguu+TRPpAlmUZavKu8JZmsZyXT6SnDSsrLFOVsyxMyTOwAGJggaNkmhm7VTiQlIA2Zsarm43UZtp1BTbQEBrSYKLijBrS60WSU8ePHSybhzbWrGi0Fweyf9+zZU+qv5eXlGS8UAXbooYdmvkM3L5nfzhxM+6C3qS1BwmuvvVYG3dT37ZKB9NZbbzUxKAcPHizVHl7rOeecI79LycLgnVKDCHZG3ZuTbMozRIO6e/fumfMwhYXuaHUPm1OTWrK1mgOD/XMlGUgEg3ieq9U8dpWS18o0FvU7+BwIEC0hVvxw+QZ4hX1AcuV2lGCoGHgIKl4dCTb8om1gIqol0SiYMGalcFNtSULuBAozbqlEZFipyiDdTlKNSY+3kzS020IJqRKZsKrrkuliI5XCrZodS/XpJ26T+99IBnWzySyvv/66XO0Jih49emQMy2wQZL9HOuCAA6Q4V6pMNhh4LQSDXe/nKksDlcxGvZzn5JGvqbvbwdhWMCj7hQFIzsHPzjjjDN4H6V06+uijM7+DzCvUr2ZVGsWA9AARyOp3Ml3F7RY89TODIdv++Prrr2VQVY1n9F9LJoJa9ReTZTMvj4wjdJHtXioG/hYNi6YhIWwCP5Aj1R09ZREs75FK4uPKz9cJyahUa0y5Gbqhp+shbMxteZPMJlJie6RqKKwSUFU7kU7T1q06CuscLa/4/19gsBtv6rVK1Js1axYmT54sddbnnntO2g8k6q6UIrm5uU1iCGr1Ly0tlQzPFff3v/99k2vZuHFjE7WAqsnnn38uvVczZ86UxL9J9PRQRdkRMNCdq+Zm5Jqg5hz//Oc/5XepWv32t7/NrKajRo1q4kLOTiXh0eVy9ea5FTAZqefv+DnBkO1oUN4w9ax5/6luCjB4+2yZPk5Ig73lJiFs8eIUoPDcfDBSq+aARmkizcDShZmyiIE6bj+VkO0jdZnabSRchUbtGhj1a2HWrRa0CqhZDtQuyyJhCNa0nYz6lWIe0nogLCx/AYa4aWWlyoo6uVGi1bnjxxV7TesnfmxQ/3dgyPYUtcQMzQGGuisNXzIgJYICA9USMjIf3ooVK6Rtoa6Ff9Pwbm5+Oynm5IpMm0MxYVslg5p37dq1ElxUHwg4AoV2DFUj5TUj0LMZV0k2ZTvQDqF3TZ2HsRLmWv3ckiH7+TidzkJ6yhSwr7nmGnHxcXe/lS/dhjrZO7WH3K/NU9AVtUP+ALPka6gyz0yXDLaJJCAEGNhpLwkBBlmjLESsbx1WPFaM0nHXoGTcFfJY9si12PpwP5Q80k+8vlLQVdg8/kpsGd8PZeL98oevEHSloMtQKt7fPP4qbB3XT3znCnksFeNKJlwpxl6DDY/fgNjKT8GMVRkkNCwgSDAYPy8YspnQ/uDsnzenVqnXZBQap8qbwnMyj4g6NSUEvTrqfYr0N998M2MztAa4ZcuWNTHAdwQMzRHnZoq50v95pJRrCZzqbwKfKeYqBkHHwbp16zIgsoOhJQNaEcFgd622BQzZ90WReK5TeH/VeCkZWKy/6qXbZdo1c4dkcpwAw9YH+srI8g6pHt5l+P7GY9DQvys8+R1Qd32OsEP2k43FKgd2ls2KXbn7imNP1BR1lgl73twe8A7oKXObKopysLWol5XmndtNjOuyLU8prxcqbv0zzO/eRMpUiYUCgCbroVOZ0tSfW01qCQzZIMhemdTfdEnaU7JpSFOnpn7OmgclxukCJDPYI7TZot/++r8Fw/bUQUaImW7C66HxyYhzc/aCnfx+v2b/HZQMSt3j5xdddFEGDMq12lL6hgKDCrplg4FGv/o9tHPs3qTsCDqv4YQTTpCxFM7HRakJGDwyS7SbTKorf+gcwLECO9T41/sdlt50BHzXd4C/eA9hg5DZWZOwG6qLNXGODvAJA92Tu7vc+Jwp4IH+nRG6rrP4uxO8rIPI64jg9b8Rtss+sg6CYwgO1jaU3XEijHUfyY3SrRU+lS75VC1mfj4wNBc0awkQLYGID4gPmGCw6+F0b3JFpUFOI1wxwB//+EepKtnjBc2RMlx3JhhIVJ1o1Cvg0rOkVvLsYKB6zToMe/CQGbgEe3OS4bzzzpO/vTmJp+IM21OTGHRT10Yw0I3akuTKNqAZk/iRZHByRRYGdNmYs618ImMHwOBagh9uPBo+Wd+wl1jR9xbz5QgA7IUaCQ7B9LldBED2koVA9el0b75fy7HCVuE2Vr7c3VCbT4mxl/wuM1953DT8r9DLvkQCVn2CtSUW66x/fjDsiHTIlhBqHI1tqgb2NAYa20pdWb16NY444ogmsYsbb7wxk5+UrZLYiZKBBvBPBYP9dzIXSOVh8ZoYvCLoCE4709qBwVgD4wsK7GQ6e3nr5ZdfnlETmSJhB0O2NCUYFLNvT03iXASD3YBWc6i/GYBTrlWOpz30IzB4CnpINUmCoWoHJYNnGRbfeAIceQcIlWcflBcchEpBNUI9KivujmphpNfm7SNe56BcqEPlRT1RWZgjU8G3FhwqALK/+LyH3AyluqA7vKyByOWGh11lv9YN954L3bsWcicgIyEbDiRlw4Fd500ikRlY58wH2RzxM/U5A180npniQBVJuS/5kGlMMzXbrm8zKsz31UOjFLn44oult4p2BedSNdb2a6BKY3fN7gzJQOL1MC6irufCCy+UCXlUb1RsgdctDNQ+dA+r2IvK26LxrLw7/J333HNPJp5BVZDlqfxNNNiVSqiuwx6BbgkM6nO7ZCCpjFeXy9WPTgpKBQUsOicI0CZgcOVaRTcZyeBYCRg74Lv3rUHFk7kIjrsAwUf/gcAjFyL4yEUIjz9XvD4bofHnWfTI2QhO+DtCE85A+JEz0DDuTITHXomGMZchNOZi+B+6CHUDe6FGgKQmv5vs6k0wrBdjjKhjtey/aoibJaRDYheCQT1QpkJQf2Y0OZvov1ZEFWjYsGGy6IfxAcVUJKY3UO3ITkegFGB9NPNo1IPmWK7KNBLpoiWoss/Humf7/D9FMtglz5o1a6Shq7J0qcbxPjHCTe8TV1imZNx0000yTUONo/eJeUB29y2JoFZgJxHArNdgjQij3awLoTeLrma6eBUIW7MZKGV4Hfw+acaMGWAWMOemS5tzqEIp1qrzN/4IDHIrKTsYdoDBmLAXUQ3GkLRlrlrpGFaJqWn1OYpDBt+o5sTE2IjQ/ROIa7oR0Bprl6N+cA9hJ1DF6gmnAIOneG9UvjEKZiIgI9ysZGP6h65yjXaCmkSmph9c3dA+ffqgurp6QWtg4Cpv1z93hFTCGJmVKodSMexMSGOatgVXVqWi2L/flvPYyz5JBINdcjDteXuSQV0XpSCDfXRLslS1tdJWHhkIpHrEhcUOdM5JiUaQECzN/aaWfh/BoCL2ar7sRL3t3XN131l3znT2nQ4GrthJGxisrW7T6RmwSPZnJQMnLVettf+DLt20bEtjxgOaZ9kccS17CNuiq6yMo9fJcePv4Zr9PLgpomxzyaZl6dRu6U3aSTaDAgNvFsFAgLSmJjGPfkfBQBHNfCaeg4xO+4Dqhd32sK/IrAtgIhxXZa5sXPWzGw5sjyiN7DXQ2WBggLA1b5JiYAJi4cKF0n6hkU6mymZcXhtVQDItUzAI6JYKhHjfKUkoYRQoWvtdCgz2+eySYXsA5TmYI0a1S9Ww73QwqNoDe7TZes+KJFv9WNOp3kY6yU4m8BE8cRldNvzVCzZMfgDB/N9Y6SEMANK7defJSK38DDL6TKDpilGsmMfOUJNoIFJsU5yS8ahb8r3WwMCVhcEpGottJTIfdW2Kentuj10q2N2l6jXVKAKHNc1MrlPzqWtu7lz0StGGUIYr56FH5/333898TvtD6fLZ1FyqOInXTalIdYRqItU1AoTqIOuvqZows9Rex2A3ZO2vCUS6O6lqqetu6feQ+JzsMQnOQRXI7nljFJ5z8PfZ52SBD2MdXHzsi85OBYO0L3Sb/p5JxUYmYW8b00ZlsE4G7AgE9k/VI5pRugibR58vbAQN3sLd4MzfXQJi69h+MCpXQ1avmdQ7reQ/awPE+E4BQ/YDV8zYLPCb8a/v6Hns86hV0878zXmMmgsi2V83d53Z32kpELW9a22JkRURaHQOUAIQsMo2aI7xWwrSZZ9je9eV/V2+RzCoREnaU1Rxs39j9rns0mqng4GT0vW5rRcqbI2A03UQXNmljWDIfKYMI0fCWv37j8M98LeoF7ZCDSPhhUIyFHRC9dv3IRWsmcXsVNNslBur65k5o9uNLP83QGiNSZpjxO0xVHMPU8ULsqvQWmJ0O1haYuqWGDn7HC2Bq7XFobXXdonW2ng7WO1pHG1ZZNT3FdBIdtfqaaed9qN6h5aCeWouYcg6hxEMzFR1CXILMHjzuqBs9HlIOVahLdVr28gq8rF8/9vAIEmqS7pUiZiBatUlCFFrWttk8TqwaR423nOOAGY3BHK7yM3XHYX7wHn771H77YdICnshwR8k7IWkzGBNfzdTXPTT+gG1xFzNPZiWHpr9pjcHsJYeiP272SpTa6Bo8n66db8qvLJ635qS5NxyMYla90626WR3wmh6jEqHh7ynce7fDWvrASsnTTy/ZEo+M5OtPVlrzucpG1CbMj1G2oXp67Da9BjWFsaGkalP4XxmktdDvqA7NmBdn8kU8pj8TKrBsojMqolR12bVrCTSlYxWn13W8JsJYUAXbzOg03GGypYA3txRGKzOYStfvl02AqvP7SakQxe5L3P56H8IMKwQ9yDV5gzTbfXNhi3r1JTxAOv1tpJN2ZZGHFPiRunxBi32wzTUTOyHLQMPFNKpg4xQO4v3R92QoxB64nIYAcfqpB7XknzQfChSxTLTDw47BQxcZWgw0zBTerwiekL4fnZ3hWxG3p6EIVGFYLkiz2NPmeZnrIugK5WfqUBWtihX8/B6lKt023VaDG5Ixk/Igis+P2tnJFPunmrGglrKXX5esn7deUbtxvNQvfZk1Czrg5oNRxj1m47Qg9Ud9fqtRyAS1BKpmCbLfBnl11mjIu55knlghpzfMC0iGDg/EzZTgplTUc+hprP8fqN6/b1m7dphEGS4y4dBPOdUg69LyrllqJEk4zN7gF5Bf1fZyjRQeXHKtfmPZpzbGAiAhWqPMurWn23UrTvKiIjxjf7OSU9VpRFzn8MKTe4rbhWVJWQZ8qDiH+UmVe7I89f0hOesFa8MtfKScjvDK4xVUsVoRqAXg9tLqVW+NdrWnRs/thH09IpF4ooEYR+kIlqkvgKuRf+Bc/SlcBYdINMx6gt2R9WN+8E59E9oeOoKRD9/ErQnuBlKUu37IDcrSfdAMrafgNdWIjPSX86oKLNGFeOxRpa9TFnKaO8YsT2Vo7moJ4lVYSyWYZmlvXKNIONnjD/QK9JcYbz9POzfymu1n4crdkJ57MTCw43oG8T9CZiWc4M9aGPl6+etfuUJlEzoh4pHrkbZ+CtR8rhYhMZdhbWPX4/UohfOr3huGGKVGwD2rBXPKCWeVVzMGZfzmrLvblL+rcsYj3z+6cVOD9e+WL/wLZQ9dyeqH74KtUzAFOcpf3csdOeGE3yrPoNTaCLRGO9jVIKVhWIhMZ933htwTHkEUW9d72TY19v32asofboQFc8WIFlR1i+55stzNr47EeHK75+iG5/qOAOw8bT9+Mgjj2Sq+2jIc7HYITAg7ipc9fJQ2RvVya1rC7qjurAXqkechMgXTyK85mM0rJ7bNlqVpuY+W/lvNK6eg/APHyGw8F2EZtyJ4Av/Qujek+EadCBq86xos1/YBwGmkd93Jtyv34rAl28g4a2q5A1LSNErwJCMamq/BrVRiuzO8RPBQKb68ssvZc499U/qnAQIszPpzqTHJplMyvJJGmf0IlGCcBXnKk/vhoqa8jUZOlsXZoSVQSbWCagUaBW4Y7IYUwQIFs7Nc9Prozpscz7lmWEUmgYjv89rYPTZHQz0i3K1FBT0+Xt73d5+Hq//rJBYVXW5lbBYmf2O590rvoDr+48RmjkWW8ZeBefUB+Fd9Bnqln4O84dZfapHXILgpsVIhpzDoo1hrZEFW/xdCQE8sWrrYW+fpLDfEqHaKclESIINeoNYpLx9Ql+8jdDQc1Hz8fNikZuB2h9mY+uq2ajY+BWSjXWa95v34Hv470LrWI1kQHy/ISDrXygJksvnIvTZO9BdvuNCa2djzYTLEZp2ByKL339Qd/tOMha/N3DDuFyEV38+Ix5tkJ0focekOhgRwPQHgzl0lRIEvH8qR2oHwODut/LlYQIM7H/aURirbAHZHa6iXqgZcTwqRp2K2hGntImqR56CqlHN0cmoEuByjDxJHE9E+fDjUHvDn1BRfDDKhU1QU9wTbgGAuvwcOAb/Ds4H+sI39R6EP3kBxubF0OMxaTDL1Ue3OmBY+zJsA0NyJ4CBxFSGJ598UjL/G2+8IXsgMdHs9ttvl25Q1iszJfmKK66QXTAIHrpf6WIkaMjUZFDGHljMkx1oYmSV0eMDDzxQBsKYv8/z0vXJFY3nveSSS2TnB7omGbHm+Xhk4TojwAQYxzAblBKFxUT05c+YOQMBv7fPyh+WYNzoezBi6G24b/hd+OoTweTSSBUqj1A1ubMqVRZsmF275emBaPju3YWJBNMfwppZMr9v/R3HofrtEfDMHA//7McQXzYZoBsyGtL0kiVonPMU3B9OhGP2i0iVrylOsJoxwa0K6iaVvT8GwSEnQS//oj8i9RoiHnGuGvGMuFVAWPMvnArPqNPgeXc0XDMnIDj/Veili0BtIbnifYTmPAK9ZOFxbFe0+a4T0PjyNaj98EE0rvv0Qv9LRSi76wz4Xh0F5+J/AzF/jpmKSztCbplg/thztkOSwYh7zlr+0p3wCMngkT79zkJCdJeVbzVCStQW9RCqS+c2EVOtmWG6jawmw+y16ioUkqdQvM7vKMlK5d4HtQIAtXLf6B6ovfkwND7RD5Hp9yG4YCpS676GEff1Zjsay5AzZNMzGnfJdGMBZTTq6ej2T5UMJK4qZFQGxJhizLx3qjTMdGTiHDMtmfLA1ZlpwIxHMG5A0FC94sOgP5uvGROwqzqUDKoPKyUQwUTmZhSW56IqxtJIllmysoyRYcYTWEvMczHKSrWNY3iNjG8w9Zk+/nUCKN9+/RX6nnYKrr36Mjz/9ER8+N47KFm9BuwsYun1kPt5y7r2knmTSp4bjPCSD4bJYiky0+YFfb2j+sA/53GkvnoBjhcHomb0XxGqLPu7sekbrHz8BjhfGSRU1+fQsPg9mK6So3Uaw4m4kDzePrVzX0LtnWdg4xu3wzFzElz/eQ0hsahFt6yFGfMKMLyL4MjTkRAqUOSz57DmiUK4ni9CrDGuuWc+gM1P/wvJjfPPCE4fg03DTkRy+lCEv3+nKO5YcEp8xlBUP3Ce4I9JiK5fBDMekhIvpaebUNhr4G2qZZvBkBLMtuyVEXIPZ7mZeV531OTvJ2sMnHk9BLG/6l5tIgmmvM42anlsedE+qM/rBX//Hmjo3x2BIYeh4YlLEZs2UjY6jpSugtng7WOIlcxK2aa6YW2uCNkNIy0NVAzDtrPnTwUDRSuTzAgGRmlfeOEFZlPKViN02S1YsABCLRnM1Z+dKAgMFt4oMHAOBnjOPPNMCQb7SkUwMDGMKhLHc06WIDLhjQxN4qovGJ72iWw1wwAbwUWwMAVaGIZyDJPg2MWCoGL3OGd1rZAID+J3hxyIe0aPxPsz38O/585GSclGSLsqTalU2sba/J9JGyYVIPztO+NkEJRGcunSvpX3ng2vY9MzQmv4Z3L5NPjvPhaNq747WZ89DmX3n4Po9FHwrpx/fP2a+U/rYUfnqHguccSkgZ2sLEXovXEIPn4hGh+7GJGJF8B574mofe5exKpWat7FU5F44O9I+vxAY90kz38mwj3mOMQj3oP0aXfB9ejViFeVDvOs+PLVpaOuTBprPn7RkN4oTzesmIz1jxUjvHHjHPbale1QWdyVjKRtlp+mFQjJUDdppTCgPUJF8uXtLhh4D7Fyd4VHMKhLSAhWvzHLtC1UlycAlNsTrgGknAy5+V7+3k0oev3uCPb/DfyDusA3/DB4xQrknfUkAivmIubaDN4Aegz0TJpFVLrULHfaNjDomWBbXPupW9raV3Dq4FSNyOzMyaf6xCJ5GtdCXXolHcU1+JrGGsFADwZVFn4mwGDQvafAoOanmkTJwCQ0ZoByfq76TBCcP3/+C1SJyOiMCLODHtMdeA6qRJQMVM+EKialB6OsNLbZrp710mPGjBWq223Yu9f+OP20c3De+f/Exf2uxeRpMxCX6TC65Q6lnk3VZsMXz2549nb4V348jiBhS55E6Td93ULl9VaV3wthI6B8dYVjwhVwfvfFufEpt8Bx1zHwPC4A+fwNKH35TqHSLLpCT6YzDeSGlrqcO9IY0WIRf07cuxE1QsrEbzke5hdvIfbFu2h88BJEwtULTKHzh796D8HbDkfM4z4wNXUUah+9BvHatfd7Vk7/63ejLgPWzPkb4kLFE3MaK2aetvbJIoS2fP18gzToBfiM9JZl+OkOFGlA//DqCGwt2l/o8DmCuqO8uJdQX3rCUZiDKmFMVxbu1yaqIhXsC4ekfSwqFH8X7YsqMX8lzyHPcyC2FB+GTUNPwtYJV6FG3ATP0plI1AhRGvGcpRuxNBAsN6zF7DHN2nLXlJ4DPb3VraEi0TsBDHZAkPmpo5PBmSZA3Z6F/GRAMrIwbqdRjeGqzGxRpiTwb2ZYUu0RK3lcSQa7V0ipSWLcFmEnzCMTU/ViDa4w2PtSXSKjsx6BIOD7PA/tFZ6nX79+UjJQRVP5RrQvKLGuufZa3D5kGA7r/Ue89dpUlKwvE1KnAl5/sA9jBnSRpkzq2DFrq6/1n36w/plb4F82bRwZWO52VLqor2fkyQhVbLyeO7WalatvdYy/CqHv5/dLTB8tJMN5SHw6EShfLvSgNeeZwdrDzZTVS0vq7DFhA/nKYAZLAO9moH4N/HOfQcXdJ6Lx6zfQuOBNeMafjWggIIAT1iJfTUb4pj8g5Hf+3vxgtLt6wrWIuNYO8a6cdfw3D1wOc/2sk82k4IOEeDbLZ169/tFr0bh++rqkLlSkFLvvRcR1J3aOZDDZHWPBDDQKHa9h8o0ITxmM0JQbEX/9BsTeGIzomzzevAN0k6AbMxQVFBHUID5rnHKb0AFHIDVrNIwvX4K5/ENxU5dAKKRBcG829maSvmoCIWXtJ8duGBL5livVVGqR3KRdz+Q+7SzJoFQaem5Gjx4t7QP6/unFIWNztaf6RH2dXh8e2XSLn7EbNxPp6OVhVidXe1aHKdUrLTHkCi/sD93j8ezNtGQm3akmXmR4rvRUnSgxWCZJpud2VHQb8nzV1dVSQjC5Tc3Lea646krM+/RTnHfe+Tj/vAtRlFeEIbcMwbx/zwG9cDQyk0K6JnjvGNwqXTh/7Ut3IbB8+p0MdMnA2Zbv+lY9cBZCVVuO11NiUapee59jUjH8K77uZ6yfi83P3oLy527Glg+ex4YPXoRetelSPZFK7wGe0jyblmHzOw9j05R7sPG1B1D60ihsnJCP8tduAMoWC1DNQPWTQh0OhAUgG7XgN1PhHv5XNASdR0dmjl9QNmkwGnylfb0r5vxl4UPXw9g09/KU9CAK6brpy6tXT8zHlpdvQdl3nwgWEPZkKiLr8HWZuPnT1GQtSVSHwprurqpM+KsXxEK1U6Kh+jFxv6BA3aR4oHZK26lmVjxQvaAp8b3aKbFg/ZhYyDksGfH1TsWC4sEwdyVu2QB03bF3q+yaZ0jbgPXNhmR4Swyq7W71DBgapNpkpP3clqv1p/VNsscDmFvDRLrnn38+04uT7lK2iqSez1plenBoI9C1yfFcwfme8v7Q701vkt1moJ3BBDICjOejx4jpzap6jQlo9GLRTUvPFP+meqa8SnxNqUVQsUBfGYl0+7722ivw+jxnfffDEjz67GO4c8QQ3DvqDnzz2RzQNQrdarspg3BUN12l37kXTkPC8W2xVOUIBndp3+C8hxHxhTswKJb0b50U/lIYy471+UbU/cfwlkUIffYogtPuhvvD8UhWLBsYlxFv8TzEYhYvW4HQzNEITr0HgQ8eRPDDMYh8+hAamfwbadAipesQ/kQY59GgVNnCW5fLMXqjt1ts5XT4vn4d8QZv50T50su8H7+EVM2au6N8tpRkDY6/BL99H8GZ4+FZMFdoew1aVMYoxPNLRK2+wT/h+f8fYsCPcG/yV5cAAAAASUVORK5CYII=" />
            </defs>
            <style>
            </style>
            <use id="Background" href="#img1" x="-1" y="0" />
          </svg>
        </div>
        <div className="block lg:hidden">
          <button className="flex items-center px-3 py-2 border rounded text-slate-900 border-slate-900 hover:text-orange-600 hover:border-orange-600" onClick={() => onToggle(e)} >
            <svg className="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" /></svg>
          </button>
        </div>
        <div className="w-full block flex-grow lg:flex lg:items-center lg:w-auto" id="toggleMenu">
          <div className="text-sm lg:flex-grow">
            <Link className='block mt-4 lg:inline-block lg:mt-0 text-slate-900 hover:text-orange-600 mr-4' to={routes.inicio()}>Inicio</Link>
            <Link className='block mt-4 lg:inline-block lg:mt-0 text-slate-900 hover:text-orange-600 mr-4' to={routes.estudiantes()}>Estudiantes</Link>
            <Link className='block mt-4 lg:inline-block lg:mt-0 text-slate-900 hover:text-orange-600 mr-4' to={routes.programas()}>Programas</Link>
          </div>
          <div>
          </div>
        </div>
      </nav>
    </header>
    <main className={mainClass} id={idBlock}>

      {
        (mainClass == 'main__estudiantes' ||
          mainClass == 'main__programas') &&

        <input type='search' className='searchInput' placeholder='Buscar...' onBlur={() => changeContent(event)} ></input>
      }


      {mainClass == 'main__estudiantes' &&

        <Link to={routes.newEstudiante()} className="rw-button rw-button-green btn-new">
          <div className="rw-button-icon ">+</div> Nuevo Estudiante
        </Link>
      }

      {mainClass == 'main__programas' &&

        <Link to={routes.newPrograma()} className="rw-button rw-button-green btn-new">
          <div className="rw-button-icon">+</div> Nuevo Programa
        </Link>
      }





      {children}
    </main>
    <footer className="p-4 bg-white rounded-lg shadow md:flex md:items-center md:justify-between md:p-6 bg-gray-500">
      <span className="text-sm text-gray-500 sm:text-center text-gray-100">© 2022 <a href="#">Esteban Villa Ramírez</a>. Prueba realizada para TsGroup.
      </span>
    </footer>


  </>
}

export default BaseLayout
