import EstudianteCell from 'src/components/Estudiante/EstudianteCell'

const EstudiantePage = ({ id }) => {
  return <EstudianteCell id={id} />
}

export default EstudiantePage
