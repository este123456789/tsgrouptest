import EditEstudianteCell from 'src/components/Estudiante/EditEstudianteCell'

const EditEstudiantePage = ({ id }) => {
  return <EditEstudianteCell id={id} />
}

export default EditEstudiantePage
