import { render } from '@redwoodjs/testing/web'

import EstudiantesPage from './EstudiantesPage'

//   Improve this test with help from the Redwood Testing Doc:
//   https://redwoodjs.com/docs/testing#testing-pages-layouts

describe('EstudiantesPage', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<EstudiantesPage />)
    }).not.toThrow()
  })
})
