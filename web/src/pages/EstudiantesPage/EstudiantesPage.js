import { Link, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'

const EstudiantesPage = () => {
  return (
    <>
      <MetaTags title="Estudiantes" description="Estudiantes page" />

      <h1>EstudiantesPage</h1>

      <p>
        Find me in
        <code>./web/src/pages/EstudiantesPage/EstudiantesPage.js</code>
      </p>

      <p>
        My default route is named
        <code>estudiantes</code>, link to me with `
        <Link to={routes.estudiantes()}>Estudiantes</Link>`
      </p>
    </>
  )
}

export default EstudiantesPage
