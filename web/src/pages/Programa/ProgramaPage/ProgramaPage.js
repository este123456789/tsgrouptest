import ProgramaCell from 'src/components/Programa/ProgramaCell'

const ProgramaPage = ({ id }) => {
  return <ProgramaCell id={id} />
}

export default ProgramaPage
