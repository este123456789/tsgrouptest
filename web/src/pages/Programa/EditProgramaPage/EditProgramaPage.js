import EditProgramaCell from 'src/components/Programa/EditProgramaCell'

const EditProgramaPage = ({ id }) => {
  return <EditProgramaCell id={id} />
}

export default EditProgramaPage
