import { Link, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'

const ProgramasPage = () => {
  return (
    <>
      <MetaTags title="Programas" description="Programas page" />

      <h1>ProgramasPage</h1>

      <p>
        Find me in
        <code>./web/src/pages/ProgramasPage/ProgramasPage.js</code>
      </p>

      <p>
        My default route is named
        <code>programas</code>, link to me with `
        <Link to={routes.programas()}>Programas</Link>`
      </p>
    </>
  )
}

export default ProgramasPage
