import { render } from '@redwoodjs/testing/web'

import ProgramasPage from './ProgramasPage'

//   Improve this test with help from the Redwood Testing Doc:
//   https://redwoodjs.com/docs/testing#testing-pages-layouts

describe('ProgramasPage', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<ProgramasPage />)
    }).not.toThrow()
  })
})
