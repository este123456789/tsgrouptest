# Prueba para TSGROUP
Realizado en  [Redwoodjs](https://redwoodjs.com/).

-React Js
-GraphQL
-Prisma
-TypeScript
-Jest
-StoryBook

## Urls 🚀


http://localhost:8910/inicio

http://localhost:8910/estudiantes

http://localhost:8910/programas



# Versionado 📌
»
«
###
1.0.0


# Prerequisitos 📌
»
«
###

-  [NodeJS](https://redwoodjs.com/).
-  [Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable).
-  [Mysql](https://dev.mysql.com/downloads/installer/).


### Instalaciòn 📋

- Instalar Nodejs
- Instalar Yarn
- Intalar Mysql
- correr comando:  yarn install
- correr comando:  npm i -S
- configure la conexion previamente creando la base de datos en el archivo /api/db/schema.prisma
- corre comando para migraciones de prisma : yarn rw prisma migrate dev
- correr comando:  yarn redwood dev

## Autor ✒️

Esteban Villa Ramirez. 2022

