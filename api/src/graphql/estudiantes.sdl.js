export const schema = gql`
  type Estudiante {
    id: Int!
    cedula: String!
    nombre: String
    email: String!
    telefono: String
    createdAt: DateTime!
    id_usuario_creacion: Int!
  }

  type Query {
    estudiantes: [Estudiante!]! @requireAuth
    estudiante(id: Int!): Estudiante @requireAuth
  }

  input CreateEstudianteInput {
    cedula: String!
    nombre: String
    email: String!
    telefono: String
    id_usuario_creacion: Int!
  }

  input UpdateEstudianteInput {
    cedula: String
    nombre: String
    email: String
    telefono: String
    id_usuario_creacion: Int
  }

  type Mutation {
    createEstudiante(input: CreateEstudianteInput!): Estudiante! @requireAuth
    updateEstudiante(id: Int!, input: UpdateEstudianteInput!): Estudiante!
      @requireAuth
    deleteEstudiante(id: Int!): Estudiante! @requireAuth
  }
`
