export const schema = gql`
  type Programa {
    id: Int!
    nombre: String
    descripcion: String!
    createdAt: DateTime!
    id_usuario_creacion: Int!
  }

  type Query {
    programas: [Programa!]! @requireAuth
    programa(id: Int!): Programa @requireAuth
  }

  input CreateProgramaInput {
    nombre: String
    descripcion: String!
    id_usuario_creacion: Int!
  }

  input UpdateProgramaInput {
    nombre: String
    descripcion: String
    id_usuario_creacion: Int
  }

  type Mutation {
    createPrograma(input: CreateProgramaInput!): Programa! @requireAuth
    updatePrograma(id: Int!, input: UpdateProgramaInput!): Programa!
      @requireAuth
    deletePrograma(id: Int!): Programa! @requireAuth
  }
`
