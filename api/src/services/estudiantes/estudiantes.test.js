import {
  estudiantes,
  estudiante,
  createEstudiante,
  updateEstudiante,
  deleteEstudiante,
} from './estudiantes'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float and DateTime types.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('estudiantes', () => {
  scenario('returns all estudiantes', async (scenario) => {
    const result = await estudiantes()

    expect(result.length).toEqual(Object.keys(scenario.estudiante).length)
  })

  scenario('returns a single estudiante', async (scenario) => {
    const result = await estudiante({ id: scenario.estudiante.one.id })

    expect(result).toEqual(scenario.estudiante.one)
  })

  scenario('creates a estudiante', async () => {
    const result = await createEstudiante({
      input: {
        cedula: 'String5368603',
        email: 'String3084278',
        id_usuario_creacion: 8179949,
      },
    })

    expect(result.cedula).toEqual('String5368603')
    expect(result.email).toEqual('String3084278')
    expect(result.id_usuario_creacion).toEqual(8179949)
  })

  scenario('updates a estudiante', async (scenario) => {
    const original = await estudiante({ id: scenario.estudiante.one.id })
    const result = await updateEstudiante({
      id: original.id,
      input: { cedula: 'String38745612' },
    })

    expect(result.cedula).toEqual('String38745612')
  })

  scenario('deletes a estudiante', async (scenario) => {
    const original = await deleteEstudiante({ id: scenario.estudiante.one.id })
    const result = await estudiante({ id: original.id })

    expect(result).toEqual(null)
  })
})
