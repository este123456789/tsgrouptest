import { db } from 'src/lib/db'

export const estudiantes = () => {
  return db.estudiante.findMany()
}

export const estudiante = ({ id }) => {
  return db.estudiante.findUnique({
    where: { id },
  })
}

export const createEstudiante = ({ input }) => {
  return db.estudiante.create({
    data: input,
  })
}

export const updateEstudiante = ({ id, input }) => {
  return db.estudiante.update({
    data: input,
    where: { id },
  })
}

export const deleteEstudiante = ({ id }) => {
  return db.estudiante.delete({
    where: { id },
  })
}
