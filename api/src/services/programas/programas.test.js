import {
  programas,
  programa,
  createPrograma,
  updatePrograma,
  deletePrograma,
} from './programas'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float and DateTime types.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('programas', () => {
  scenario('returns all programas', async (scenario) => {
    const result = await programas()

    expect(result.length).toEqual(Object.keys(scenario.programa).length)
  })

  scenario('returns a single programa', async (scenario) => {
    const result = await programa({ id: scenario.programa.one.id })

    expect(result).toEqual(scenario.programa.one)
  })

  scenario('creates a programa', async () => {
    const result = await createPrograma({
      input: { descripcion: 'String545979', id_usuario_creacion: 213988 },
    })

    expect(result.descripcion).toEqual('String545979')
    expect(result.id_usuario_creacion).toEqual(213988)
  })

  scenario('updates a programa', async (scenario) => {
    const original = await programa({ id: scenario.programa.one.id })
    const result = await updatePrograma({
      id: original.id,
      input: { descripcion: 'String42022672' },
    })

    expect(result.descripcion).toEqual('String42022672')
  })

  scenario('deletes a programa', async (scenario) => {
    const original = await deletePrograma({ id: scenario.programa.one.id })
    const result = await programa({ id: original.id })

    expect(result).toEqual(null)
  })
})
