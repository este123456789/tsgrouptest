import { db } from 'src/lib/db'

export const programas = () => {
  return db.programa.findMany()
}

export const programa = ({ id }) => {
  return db.programa.findUnique({
    where: { id },
  })
}

export const createPrograma = ({ input }) => {
  return db.programa.create({
    data: input,
  })
}

export const updatePrograma = ({ id, input }) => {
  return db.programa.update({
    data: input,
    where: { id },
  })
}

export const deletePrograma = ({ id }) => {
  return db.programa.delete({
    where: { id },
  })
}
